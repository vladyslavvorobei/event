// import $ from 'jquery';
import '../../node_modules/slick-carousel/slick/slick.min';
import '../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import '../../node_modules/jquery-popup-overlay/jquery.popupoverlay';
import '../../node_modules/jquery-mask-plugin/dist/jquery.mask.min';
import '../../node_modules/gsap/src/minified/TweenMax.min';
import Swiper from 'swiper';
import '../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min';
// import '../../node_modules/jquery-validation/dist/jquery.validate.min';
// import '../../src/js/lib/simplebar';
// import '../../node_modules/ion-rangeslider/js/ion.rangeSlider.min';
// import '../../node_modules/air-datepicker/dist/js/datepicker.min';
// - - - - SCROLL-BAR - - - - OPEN
$(window).on('load',function() {
  $('.reviews__slider_text').mCustomScrollbar({
    theme: "minimal-dark"
  });

  $('.press-release__text').mCustomScrollbar({
  });
  // $('.reviews__instagram-slider_link').mCustomScrollbar({
  // });
});
// - - - - SCROLL-BAR - - - - CLOSE

// - - - - MENU-GSAP-ANIMATION - - - - OPEN

var t1 = new TimelineMax({paused: true});

t1.to(".overlay", 1, {

  top: 0,
  ease: Expo.easeInOut

});

t1.staggerFrom(".menu ul li", 1, {y: 100, opacity: 0, ease: Expo.easeOut}, 0.1);

t1.reverse();
$(document).on('click', '.menu-btn__wrapper', function() {
  t1.reversed(!t1.reversed());
  $('.menu-btn').toggleClass('active');
  $('.logo').toggleClass('active');
  $('.header__wrapper').toggleClass('active');
});

t1.reverse();
$(document).on('click', '.nav ul li a', function() {
  t1.reversed(!t1.reversed());
  $('.menu-btn').toggleClass('active');
  $('.logo').toggleClass('active');
  $('.header__wrapper').toggleClass('active');
});



// - - - - MENU-GSAP-ANIMATION - - - - CLOSE

// - - - - WINDOW-SCROLL - - - - OPEN

$(window).scroll(function() {
  fixedHeader();
});

var fixedHeader = function () {
  if($(window).scrollTop() > 70) {
    $('.logo').addClass('scroll');
    $('.header').addClass('scroll');
  }
  else {
    $('.logo').removeClass('scroll');
    $('.header').removeClass('scroll');
  }
};

fixedHeader();

// - - - - WINDOW-SCROLL - - - - CLOSE

// - - - - ADD SLIDERS - - - - OPEN
 // - - - - VIDEO-GALLERY-SLIDER - - - - OPEN
var swiper = new Swiper('.video-gallery__slider .swiper-container', {
  slidesPerView: 3,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    991: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    767: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    565: {
      slidesPerView: 1,
      spaceBetween: 20,
    }
  }
});
 // - - - - VIDEO-GALLERY-SLIDER - - - - CLOSE
    // - - - - SWIPER-SLIDER-FIRST-SCREEN - - - - OPEN
var swiper = new Swiper('.first-screen__slider .swiper-container', {
  slidesPerView: 1,
  spaceBetween: 0,
  loop: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

    // - - - - SWIPER-SLIDER-FIRST-SCREEN - - - - CLOSE

    // - - - - SWIPER-SLIDER-ABOUT-LICENSES - - - - OPEN
      var swiperLicenses = new Swiper('.licenses__slider .swiper-container', {
        slidesPerView: 4,
        spaceBetween: 40,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
        breakpoints: {
          1199: {
            slidesPerView: 3,
            spaceBetween: 20,
          },
          991: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          767: {
            slidesPerView: 2,
            spaceBetween: 30,
          },
          565: {
            slidesPerView: 1,
            spaceBetween: 20,
          }
        }
      });
    // - - - - SWIPER-SLIDER-ABOUT-LICENSES - - - - CLOSE

    // - - - - SLICK-SLIDER-PARTNERS - - - - OPEN

$('.partners-slider').slick({
  infinite: true,
  slidesToShow: 8,
  arrows: false,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 6,
        dots: true
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
        dots: true
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 3,
        dots: true
      }
    },
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 1,
        dots: true
      }
    },
  ]
});

    // - - - - SLICK-SLIDER-PARTNERS - - - - CLOSE

    // - - - - SWIPER-SLIDER-REVIEWS - - - - OPEN
    // - - - - INSTAGRAM-SLIDER - - - -
  var swiperInstagram = new Swiper('.reviews__instagram-slider.swiper-container', {
    spaceBetween: 30,
    // mousewheel: true,
    pagination: {
      el: '.swiper-pagination',
    },
  });
    // - - - - REVIEWS-SLIDER - - - -
  var swiperReviews = new Swiper('.reviews__slider.swiper-container', {
    slidesPerView: 2,
    spaceBetween: 30,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      1199: {
        slidesPerView: 1,
      },
      991: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
    }
  });
    // - - - - SWIPER-SLIDER-REVIEWS - - - - CLOSE

// - - - - ADD SLIDERS- - - - CLOSE


// - - - - WINDOW.RESIZE - - - - OPEN

$(window).resize(function () {
  swiperInstagram.update();
  $(".main-title").each(function(i, elem) {
    if ($(elem).text().length <= 20) {
      $(elem).css('fontSize', '108px');
    } else if ($(elem).text().length <= 28){
      $(elem).css('fontSize', '78px');
    } else if ($(elem).text().length <= 143) {
      $(elem).css('fontSize', '50px');
    } else {
      $(elem).css('fontSize', '36px');
    }
  });
  if ($(window).width() < 1199) {
    $(".main-title").each(function(i, elem) {
      if ($(elem).text().length <= 20) {
        $(elem).css('fontSize', '78px');
      } else if ($(elem).text().length <= 28){
        $(elem).css('fontSize', '50px');
      } else if ($(elem).text().length <= 143) {
        $(elem).css('fontSize', '36px');
      } else {
        $(elem).css('fontSize', '24px');
      }
    });
  }
  if ($(window).width() < 767){
    $(".main-title").each(function (i, elem) {
      if ($(elem).text().length <= 20) {
        $(elem).css('fontSize', '36px');
      } else if ($(elem).text().length <= 28) {
        $(elem).css('fontSize', '24px');
      } else {
        $(elem).css('fontSize', '18px');
      }
    })}
});

// - - - - WINDOW.RESIZE - - - - CLOSE


// - - - - MAIN-TITLE-SIZE - - - - OPEN

$(window).ready(function() {
  $(".main-title").each(function(i, elem) {
    if ($(elem).text().length <= 20) {
      $(elem).css('fontSize', '108px');
    } else if ($(elem).text().length <= 28){
      $(elem).css('fontSize', '78px');
    } else if ($(elem).text().length <= 143) {
      $(elem).css('fontSize', '50px');
    } else {
      $(elem).css('fontSize', '36px');
    }
  });
  if ($(window).width() < 1199) {
    $(".main-title").each(function(i, elem) {
      if ($(elem).text().length <= 20) {
        $(elem).css('fontSize', '78px');
      } else if ($(elem).text().length <= 28){
        $(elem).css('fontSize', '50px');
      } else if ($(elem).text().length <= 143) {
        $(elem).css('fontSize', '36px');
      } else {
        $(elem).css('fontSize', '24px');
      }
    });
  }
  if ($(window).width() < 767){
      $(".main-title").each(function (i, elem) {
        if ($(elem).text().length <= 20) {
          $(elem).css('fontSize', '36px');
        } else if ($(elem).text().length <= 28) {
          $(elem).css('fontSize', '24px');
        } else {
          $(elem).css('fontSize', '18px');
        }
    })}

});
// new SimpleBar($('.reviews__instagram-slider_link')[0]);


// - - - - MAIN-TITLE-SIZE - - - - CLOSE
$(document).ready(function() {

  $('#venues a').click(function (e) {
    e.preventDefault();
    $('#venues a').removeClass('active');
    $(this).toggleClass('active');
  });

  // - - - - RANGE-SLIDER-FILTER - - - - OPEN

  // - - - - RANGE-SLIDER-FILTER - - - - CLOSE



  // - - - - ADD-TABS-ABOUT-TEMPLATE - - - - OPEN
  $('.about-tabs-content').not(':first').hide();
  $('.about-tabs-nav').click(function() {
    $('.about-tabs-nav').removeClass('active').eq($(this).index()).addClass('active');
    $('.about-tabs-content').hide().eq($(this).index()).fadeIn();
  }).eq(0).addClass('active');
  // - - - - ADD-TABS-ABOUT-TEMPLATE - - - - CLOSE
  // - - - - FAQ-SCREEN - - - - OPEN
  // - - - - FAQ-BURGER - - - - OPEN

  $('.faq__head').click(function () {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
    } else {
      $('.faq__head').removeClass('active');
      $(this).addClass('active');
    }
    $(this).next().slideToggle();
    $('.faq__toggle').not($(this).next()).slideUp();
  });
  // - - - - FAQ-BURGER - - - - CLOSE

  // - - - - FAQ-SCREEN - - - - CLOSE


  // - - - - MAP-CONTROL - - - - OPEN

  var mapControl = document.getElementById('map') !== null ? initMap() : null;

  // - - - - MAP-CONTROL - - - - CLOSE
  // - - - - FILTER - - - - OPEN
  $('.filter-title').click(function () {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
    } else {
      $('.filter-title').removeClass('active');
      $(this).addClass('active');
    }
    $(this).next().slideToggle();
    $('.controls').not($(this).next()).slideUp();
  });

  $('.clear-filter').click(function () {
    $('.filter-title').removeClass('active');
    $('.controls').not($(this).next()).slideUp();
  });

  // - - - - FILTER - - - - CLOSE

  //- - - - AIR-DATAPICKER-FILTER - - - - OPEN
  //
  // $('.datepicker-here').datepicker({
  //   range: true,
  //   toggleSelected: false,
  //   inline: true,
  //   multipleDatesSeparator: ' - ',
  //   onSelect: function (formattedDate, date, inst) {
  //     $('input[name="date_from"]').add().val();
  //   }
  // });
  //- - - - AIR-DATAPICKER-FILTER - - - - CLOSE

  // - - - - RADIO-FILTER - - - - OPEN

    var radios = document.querySelectorAll('input[name=data-filter]'),
      radioChk;
    [].forEach.call(radios, function(elem) {
      elem.onclick = function() {
        if (radioChk !== elem) {
          radioChk = elem;
        } else {
          elem.checked = false;
          radioChk = '';
        }
      }
    });

  // - - - - RADIO-FILTER - - - - CLOSE

  // - - - - FILTER-RANGE - - - - OPEN
  // $(".js-range-slider").ionRangeSlider({
  //   type: "double",
  //   min: 500,
  //   max: 5000,
  //   from: 500,
  //   to: 5000,
  //   grid: true,
  //   step: 500,
  //   from_fixed: true,
  //   onFinish: function (data) {
  //     $('input[name="price"]').val(data.to);
  //     $('.price label').html('До ' + data.to + ' руб.');
  //   },
  // });
  // - - - - FILTER-RANGE - - - - CLOSE

  // - - - - VENUES-POPUP - - - - OPEN
  $('.modal, #venues, #thanks').popup({
    transition: 'all 0.3s',
    outline: true,
    focusdelay: 400,
    vertical: 'top',
    closebutton: true
  });
  // -- -- -- MODAL-POPUP -- -- -- END
  // -- -- -- jQuery Mask + jquery VALIDATION script BEGIN -- -- --
  $('input[type="tel"]').mask('+7 (000) 000-00-00');
  // jQuery.validator.addMethod('phoneno', function(phone_number, element) {
  //   return this.optional(element) || phone_number.match(/\+[0-9]{1}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/);
  // }, 'Введите Ваш телефон');

  // $('.form').each(function(index, el) {
  //   $(el).addClass('form-' + index);
  //
  //   $('.form-' + index).validate({
  //     rules: {
  //       name: 'required',
  //       country: 'required',
  //       formName: 'required',
  //       email: 'required',
  //       tel: {
  //         required: true,
  //         phoneno: true
  //       }
  //     },
  //     messages: {
  //       name: 'Введите Ваше имя',
  //       tel: 'Введите Ваш телефон',
  //       email: 'Введите Ваш e-mail'
  //     },
  //     submitHandler: function(form) {
  //       var t = $('.form-' + index).serialize();
  //       ajaxSend('.form-' + index, t);
  //     }
  //   });
  // });
  function ajaxSend(formName, data) {
    jQuery.ajax({
      type: 'POST',
      url: 'sendmail.php',
      data: data,
      success: function() {
        $('.modal, .modal-callback, .modal-review').popup('hide');
        $('#thanks').popup('show');
        setTimeout(function() {
          $(formName).trigger('reset');
        }, 2000);
      }
    });
  };
  // - - - - VENUES-POPUP - - - - CLOSE
});

